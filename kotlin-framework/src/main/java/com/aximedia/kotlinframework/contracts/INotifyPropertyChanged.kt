package com.aximedia.kotlinframework.contracts

interface INotifyPropertyChanged {
    fun propertyChanged(propertyName : String)
}